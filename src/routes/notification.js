const express = require("express");
const notificationController = require("../controllers/notification");

const router = express.Router();

router
  .route("/")
  .get(notificationController.getNotification)
  .post(notificationController.createNotification);
  
router
  .route("/:id")
  .get(notificationController.getNotificationById)

module.exports = router;