const express = require("express");
const router = express.Router();
const publicationRoutes = require('./publication');
const notificationRoutes = require('./notification');
const reactionRoutes = require('./reaction');
const commentRoutes = require('./comment');

router.use("/publication", publicationRoutes);
router.use("/notification", notificationRoutes);
router.use("/reaction", reactionRoutes);
router.use("/comment", commentRoutes);

module.exports = router;