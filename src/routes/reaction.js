const express = require("express");
const reactionController = require("../controllers/reaction");

const router = express.Router();
  
router
  .route("/:pubId")
  .post(reactionController.createReaction);

module.exports = router;