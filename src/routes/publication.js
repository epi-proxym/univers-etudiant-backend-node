const express = require("express");
const publicationController = require("../controllers/publication");

const router = express.Router();

router
  .route("/")
  .get(publicationController.getPublications)
  .post(publicationController.createPublication);
  
router
  .route("/:id")
  .get(publicationController.getPublicationById)

module.exports = router;