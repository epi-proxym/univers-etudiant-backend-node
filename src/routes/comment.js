const express = require("express");
const commentController = require("../controllers/comment");

const router = express.Router();
  
router
  .route("/:pubId")
  .post(commentController.createComment);

module.exports = router;