var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var publicationSchema = new Schema({
  content:String,
  userId: String,
  comments: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Comment' }],
  reactions: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'Reaction' }]
});

var Publication = mongoose.model('Publication', publicationSchema);

module.exports = Publication;