var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reactionSchema = new Schema({
  type:{
    type: String,
    enum: ['LIKE', 'DISLIKE']
  },
  userId: String,
  publication: { type: mongoose.SchemaTypes.ObjectId, ref: 'Publication' }
});

var Reaction = mongoose.model('Reaction', reactionSchema);

module.exports = Reaction;
