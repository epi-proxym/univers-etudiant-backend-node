var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = new Schema({
  content:String,
  redirectUrl: String
});

var Notification = mongoose.model('Notification', notificationSchema);

module.exports = Notification;