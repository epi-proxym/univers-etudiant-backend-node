var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
  content: String,
  userId: String,
  publication: { type: mongoose.SchemaTypes.ObjectId, ref: 'Publication' }
});

var Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;
