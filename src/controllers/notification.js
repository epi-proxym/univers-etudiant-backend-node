const Notification = require('../models/notification')

const getNotification = (req, res) => {
    return Notification.find()
        .then(notifications => res.json(notifications));
}

const createNotification = (req, res) => {
    return Notification.create(req.body)
        .then(notifications => res.json(notifications));
}

const getNotificationById = (req, res) => {
    return Notification.findById(req.params.id)
        .then(notifications => res.json(notifications));
}

module.exports = {
    getNotification,
    createNotification,
    getNotificationById
}
