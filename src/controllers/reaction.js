const Reaction = require('../models/reaction')
const Publication = require('../models/publication');

const createReaction = async (req, res) => {
    const newReaction = new Reaction({
      ...req.body, 
      publication: req.params.pubId
    }) 
    let [_reaction] = await Promise.all([
      newReaction.save(),
      Publication.findByIdAndUpdate(req.params.pubId, {
        $push: {
          reactions: newReaction._id
        }
      })
    ])
    res.status(200).send(_reaction);
}

module.exports = {
    createReaction,
}
