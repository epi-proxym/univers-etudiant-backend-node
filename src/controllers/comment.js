const Comment = require('../models/comment')
const Publication = require('../models/publication');

const createComment = async (req, res) => {
  const newComment = new Comment({
    ...req.body, 
    publication: req.params.pubId
  }) 
  let [_comment] = await Promise.all([
    newComment.save(),
    Publication.findByIdAndUpdate(req.params.pubId, {
      $push: {
        comments: newComment._id
      }
    })
  ])
  res.status(200).send(_comment);
}

module.exports = {
    createComment,
}
