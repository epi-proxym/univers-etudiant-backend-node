const Publication = require('../models/publication')
const Comment = require('../models/comment');
const Reaction = require('../models/reaction');

const getPublications = (req, res) => {
    return Publication.find({})
        .populate('comments', Comment )
        .populate('reactions')
        .exec()
        .then(publications => res.json(publications));
}

const createPublication = (req, res) => {
    return Publication.create(req.body)
        .then(publication => res.json(publication));
}

const getPublicationById = (req, res) => {
    return Publication.findById(req.params.id)
        .then(publication => res.json(publication));
}

module.exports = {
    getPublicationById,
    createPublication,
    getPublications
}
