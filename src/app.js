const mongoose = require('mongoose');
mongoose.connect(process.env.DB);
const bodyParser = require('body-parser');
const express = require("express");
const app = express();
const PORT = 3000;

const routes = require('./routes');

app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

app.use('/', routes)

app.listen(PORT, () => {
    console.log('Server is up and running on port numner ' + PORT);
});