# Stage 0, "build-stage", based on Node.js 10, to build and compile Express
FROM node:10 as build-stage

RUN mkdir -p /app

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY ./ /app/

RUN npm run build

# Stage 1, based on Node.js 10-alpine, to have only the bundle, ready for production
FROM node:10-alpine

RUN mkdir -p /app

WORKDIR /app

COPY package*.json /app/

RUN apk add --no-cache make gcc g++ python && \
    npm install --production --silent && \
    apk del make gcc g++ python && \
    npm cache clean --force

COPY --from=build-stage /app/api.bundle.js .
COPY --from=build-stage /app/mail-templates ./mail-templates

EXPOSE 3000

CMD [ "node", "api.bundle.js"]
