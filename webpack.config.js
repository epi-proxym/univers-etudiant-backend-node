const path = require("path");
var nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: "./src/app.js",
  output: {
    publicPath: "/",
    path: path.resolve(__dirname),
    filename: "api.bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/
      }
    ]
  },
  mode: "production",
  target: "node",
  node: {
    __dirname: false,
    __filename: false
  },
  externals: [nodeExternals()]
};
